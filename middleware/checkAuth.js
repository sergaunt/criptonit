const jwt = require('jsonwebtoken');
const CONFIG = require('../config/config');
// const { TokenBlackList } = require("../models/index");

module.exports = (req, res, next) => {
  if (typeof req.cookies !== 'undefined' && typeof req.cookies.token !== 'undefined') {
    const token = req.cookies.token;
    jwt.verify(token, CONFIG.jwt_encryption, (err, authData) => {
      if (err) {
        res.render('signup');
      } else {
        next();
      }
    })
  } else {
    res.render('signup');
  }
  // const bearerHeader = req.headers['authorization'];
  // if (typeof bearerHeader !== 'undefined') {
  //   const token = bearerHeader.split(' ')[1];
  //   jwt.verify(token, CONFIG.jwt_encryption, (err) => {
  //     TokenBlackList.findAll()
  //       .then(data => data.map(obj => obj.token))
  //       .then(tokens => {
  //         if (err || tokens.includes(token)) {
  //           res.sendStatus(403);
  //         } else {
  //           next();
  //         }
  //       });
  //   });
  // } else {
  //   res.sendStatus(403);
  // }
};
