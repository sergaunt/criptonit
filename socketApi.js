const socket = require('socket.io');
const io = socket();
const socketApi = {};
const { Message } = require('./models/index');


/**
 * Implement Socket.IO
 */
io.on('connection', (socket) => {
  socket.on('chat', (data) => {
    const { user, message, time } = data;
    io.emit('chat', data);
    Message.create({user, message, time});
  });

  socket.on('typing', (data) => {
    socket.broadcast.emit('typing', data);
  });
});

socketApi.io = io;

module.exports = socketApi;