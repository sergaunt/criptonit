const { User, TokenBlackList } = require("../models/index");
const { hash, compare } = require('bcryptjs');
const jwt = require('jsonwebtoken');
const CONFIG = require('../config/config');

// User registration
exports.register = async function(req, res, next) {
  const { name, email, password, passwordConfirm } = req.body;
  let errors = [];
  const data = await User.findAll();
  const emails = await data.map(obj => obj.email);
  if (!name && name.length < 2) {
    errors.push({ text: 'Name should have at least 2 characters'})
  }
  if (emails.includes(email)) {
    errors.push({ text: 'Email already in use' });
  }
  if (password !== passwordConfirm) {
    errors.push({ text: 'Passwords don\'t match' });
  }
  if (password.length < 5) {
    errors.push({ text: 'Password should have at least 8 characters' });
  }
  if (errors.length > 0) {
    res.render('signup', { errors, email, password });
  } else {
    hash(password, 10)
      .then((hashed) => User.create({ name, email, password: hashed }))
      .then((user) => {
        const token = jwt.sign({
          name: user.name,
          email: user.email,
          id: user.id
        }, CONFIG.jwt_encryption, {
          expiresIn: "1h"
        });
        res.cookie('token', token, { httpOnly: true });
        // res.json({ user, token });
        res.redirect('/');
      })
      .catch((err) => {
        res.send(err);
      });
  }
};

// User authorization
exports.login = (req, res, next) => {
  const { email, password } = req.body;
  let errors = [];
  User.findOne({ where: { email } })
    .then((user) => {
      if (!user) {
        errors.push({ text: 'Account not found' });
      } else if (!compare(password, user.password)) {
        errors.push({ text: 'Wrong password' });
      }
      if (errors.length > 0) {
        res.render('login', { errors, email, password });
      } else {
        const token = jwt.sign({
          name: user.name, 
          email: user.email,
          id: user.id
        }, CONFIG.jwt_encryption, {
          expiresIn: "1h"
        });
        res.cookie('token', token, { httpOnly: true });
        // res.json({ user, token });
        res.redirect('/');
      }
    });
}

// User logout
exports.logout = (req, res, next) => {
  res.clearCookie('token');
  res.redirect('/signup');
  // const bearerHeader = req.headers['authorization'];
  // const token = bearerHeader.split(' ')[1];
  // TokenBlackList.findAll()
  //   .then(expiredTokens => {
  //     const tokens = expiredTokens.map(obj => obj.token);
  //     if (tokens.includes(token)) {
  //       return res.json({
  //         message: "Logged out"
  //       })
  //     } else {
  //       TokenBlackList.create({ token })
  //         .then((token) => {
  //           res.json({
  //             message: "Logged out",
  //             token
  //           })
  //         });
  //     }
  //   });
}