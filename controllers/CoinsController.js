const request = require('request-promise');
const jwt = require('jsonwebtoken');
const { Message } = require('../models/index');
const Op = require('sequelize').Op;
// Get list of coins
const decodeUserName = (req) => jwt.decode(req.cookies.token)['name'];

const getMessageHistory = async () => {
  return await Message.findAll({
    where: {
      time: {
        [Op.gte]: Date.now() - 1800000 // half-hour later
      }
    }
  }).then(data => data.map(obj => ({
      user: obj.user,
      message: obj.message,
      time: obj.time.toLocaleTimeString()
    }))  
  )};

exports.getCoinsList = (req, res, next) => {
  getMessageHistory()
    .then(messages => {
      const userName = decodeUserName(req);
      const requestOptions = {
        method: 'GET',
        uri: 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&per_page=15',
        json: true
      };
      request(requestOptions)
        .then((dataJSON) => {
          const data = dataJSON.map((obj) => {
            const {id, name, symbol, image, current_price} = obj;
            return { 
              id, 
              name, 
              symbol: symbol.toUpperCase(), 
              image, 
              currentPrice: current_price.toFixed(2) 
            }
          });
          // console.log(messages);     
          res.render('index', { data, logged: true, userName, messages });
        })
    })
    .catch(err => {
      console.log(err);
    });
};

// Get coin details
exports.getCoinDetails = (req, res, next) => {
  // const messageHistory = getMessageHistory();
  getMessageHistory()
    .then(messages => {
      const userName = decodeUserName(req);
      const requestOptions = {
        method: 'GET',
        uri: `https://api.coingecko.com/api/v3/coins/${req.params.id}`,
        json: true
      };
      request(requestOptions)
        .then((dataJSON) => {
          const {id, name, symbol} = dataJSON,
            description = dataJSON.description.en,
            homePage = dataJSON.links.homepage[0],
            image = dataJSON.image.large,
            genesisDate = dataJSON['genesis_date'],
            currentPrice = dataJSON['market_data']['current_price'];
          res.render('coin', { id, name, symbol, genesisDate, description, homePage, image, currentPrice, logged: true, userName, messages });
        })
    })
    .catch(err => {
      console.log(err);
    });
};