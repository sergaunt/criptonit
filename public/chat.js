const socket = io.connect('http://localhost:3000/');

const chat = document.getElementById('chat'),
  message = document.getElementById('message'),
  btn = document.getElementById('send'),
  output = document.getElementById('output'),
  feedback = document.getElementById('feedback'),
  closeButton = document.getElementById('close-chat'),
  openButton = document.getElementById('open-chat'),
  user = document.getElementById('userName').innerText || 'Anonymous';

// open and close chats
closeButton.addEventListener('click', (e) => {
  chat.style.display = 'none';
  openButton.style.display = 'flex';
});

openButton.addEventListener('click', (e) => {
  chat.style.display = 'flex';
  openButton.style.display = 'none';
});

// emit events
chat.addEventListener('submit', (e) => {
  e.preventDefault();
  if (message.value.length > 0) {
    socket.emit('chat', {
      user,
      message: message.value,
      time: Date.now()
    });
    message.value = '';
  }
});

const typingTimeout = setTimeout.bind(null, () => {
  feedback.innerHTML = '';
}, 2500);

message.addEventListener('keypress', () => {
  socket.emit('typing', user);
})

// listen for events
socket.on('chat', (data) => {
  feedback.innerHTML = '';
  const date = new Date();
  date.setTime(data.time);
  const dataFormatted = date.toLocaleTimeString();
  output.innerHTML += `<p class="message-container"><span class="message-text">${data.message}</span><span class="message-info"><strong>${data.user}</strong>${dataFormatted}</span></p>`;
});

socket.on('typing', (data) => {
  clearTimeout(typingTimeout);
  feedback.innerHTML = `<em>${data} is typing...</em>`;
  typingTimeout();
});