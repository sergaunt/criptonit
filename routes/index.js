const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/checkAuth');
const UserController = require('../controllers/UserController');
const CoinsController = require('../controllers/CoinsController');

/* GET signup page */
router.get('/signup', (req, res, next) => {
  console.log(req.headers.cookie);
  if (!req.cookies || !req.cookies.token ) {
    res.render('signup', { logged: false });
  } else {
    res.redirect('/');
  }
});

/* GET login page. */
router.get('/login', (req, res, next) => {
  if (!req.cookies || !req.cookies.token) {
    res.render('login', { logged: false });
  } else {
    res.redirect('/');
  }
});

/* POST signup page */
router.post('/signup', UserController.register);

/* POST login into account */
router.post('/login', UserController.login);

/* GET logout from the account */
router.get('/logout', UserController.logout);

/* GET home page. */
router.get('/', checkAuth, CoinsController.getCoinsList);

/* GET coin page */
router.get('/coins/:id', checkAuth, CoinsController.getCoinDetails);

module.exports = router;
