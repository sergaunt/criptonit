const Sequelize = require('sequelize');
const CONFIG = require('../config/config');

const sequelize = new Sequelize(CONFIG.db_name, CONFIG.db_user, CONFIG.db_password, {
  host: CONFIG.db_host,
  dialect: CONFIG.db_dialect,
  port: CONFIG.db_port,
  operatorsAliases: false,
  timezone: '+08:00' // WTF?
});

const UserSchema = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
    validate: {
      isEmail: true
    }
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
    validate: {
      len: {
        args: [8, 255],
        msg: 'Password should have at least 8 characters'
      }
    }
  }
};

const TokenBlacklistSchema = {
  token: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true
  }
};

const MessagesSchema = {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  user: {
    type: Sequelize.STRING,
    allowNull: false
  },
  message: {
    type: Sequelize.TEXT('tiny'),
    allowNull: false
  },
  time: {
    type: Sequelize.DATE
  }
};

const User = sequelize.define('user', UserSchema, { timestamps: false });
const TokenBlackList = sequelize.define('expired_token', TokenBlacklistSchema, { timestamps: false });
const Message = sequelize.define('message', MessagesSchema, { timestamps: false });

module.exports = {
  User,
  TokenBlackList,
  Message
};